//
//  ContentView.swift
//  Persistence
//
//  Created by Tomáš Pařízek on 19.11.2023.
//

import SwiftUI
import CoreData

enum ProviderType {
    case userDefaults
    case keychain
    case coreData
    case realm

    var provider: PersistenceProvider {
        switch self {
        case .userDefaults:
            return UserDefaultsProvider()
        case .keychain:
            return KeychainProvider()
        case .coreData:
            return CoreDataProvider()
        case .realm:
            return RealmProvider()
        }
    }
}

enum TestType {
    case sequential
    case grouped
}

struct ContentView: View {

    @State private var providerType: ProviderType = .userDefaults
    @State private var testType: TestType = .sequential
    @State private var numberOfRecords: Int = 10000
    @State private var testResult: String = "Test not started"

    var body: some View {
        VStack {
            Picker("Provider", selection: $providerType) {
                Text("UserDefaults").tag(ProviderType.userDefaults)
                Text("Keychain").tag(ProviderType.keychain)
                Text("CoreData").tag(ProviderType.coreData)
                Text("Realm").tag(ProviderType.realm)
            }
            .pickerStyle(SegmentedPickerStyle())

            Picker("Test", selection: $testType) {
                Text("Sequential").tag(TestType.sequential)
                Text("Grouped").tag(TestType.grouped)
            }
            .pickerStyle(SegmentedPickerStyle())

            TextField("Number of records", value: $numberOfRecords, formatter: NumberFormatter())
                .keyboardType(.numberPad)
                .textFieldStyle(RoundedBorderTextFieldStyle())

            Spacer()

            Button("Run test") {
                testResult = "Test running..."

                let records = (0..<numberOfRecords).reduce(into: [String: String]()) { result, i in
                    result["key\(i)"] = "value\(i)"
                }

                let startTime = DispatchTime.now()

                switch testType {
                case .sequential:
                    let provider = providerType.provider
                    records.forEach { key, value in
                        provider.save(key: key, value: value)
                    }

                case .grouped:
                    let provider = providerType.provider
                    provider.save(values: records)
                }

                let endTime = DispatchTime.now()
                let elapsedTime = endTime.uptimeNanoseconds - startTime.uptimeNanoseconds
                let elapsedTimeSeconds = Double(elapsedTime) / 1_000_000_000

                testResult = "Test completed in \(elapsedTimeSeconds) s"
            }

            Text(testResult)
        }
        .padding()
    }

}
