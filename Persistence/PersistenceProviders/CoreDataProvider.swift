//
//  DatabaseProvider.swift
//  Persistence
//
//  Created by Tomáš Pařízek on 19.11.2023.
//

import Foundation

struct CoreDataProvider: PersistenceProvider {
    func save(key: String, value: String) {
        let context = PersistenceController.shared.container.viewContext
        context.performAndWait {
            let entity = PersistedEntity(context: context)
            entity.key = key
            entity.value = value
            context.insert(entity)
            try! context.save()
        }
    }

    func save(values: [String: String]) {
        let context = PersistenceController.shared.container.viewContext
        context.performAndWait {
            values.forEach { key, value in
                let entity = PersistedEntity(context: context)
                entity.key = key
                entity.value = value
                context.insert(entity)
            }

            try! context.save()
        }
    }

    func read(key: String) -> String? {
        let context = PersistenceController.shared.container.viewContext
        
        let request = PersistedEntity.fetchRequest()
        request.predicate = NSPredicate(format: "key == %@", key)
        
        let result = try! context.fetch(request)

        return result.first?.value
    }
}
