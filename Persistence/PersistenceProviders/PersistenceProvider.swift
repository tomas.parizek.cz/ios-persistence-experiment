//
//  PersistenceProvider.swift
//  Persistence
//
//  Created by Tomáš Pařízek on 19.11.2023.
//

import Foundation

protocol PersistenceProvider {
    func save(key: String, value: String)
    func save(values: [String: String])
    func read(key: String) -> String?
}
