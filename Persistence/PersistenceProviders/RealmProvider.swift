//
//  RealmProvider.swift
//  Persistence
//
//  Created by Tomáš Pařízek on 19.11.2023.
//

import Foundation
import RealmSwift

class RealmPersistedEntity: Object {
    @Persisted var key: String
    @Persisted var value: String
}

struct RealmProvider: PersistenceProvider {
    func save(key: String, value: String) {
        let realm = try! Realm()
        try! realm.write {
            let entity = RealmPersistedEntity()
            entity.key = key
            entity.value = value
            realm.add(entity)
        }
    }

    func save(values: [String: String]) {
        let realm = try! Realm()
        try! realm.write {
            values.forEach { key, value in
                let entity = RealmPersistedEntity()
                entity.key = key
                entity.value = value
                realm.add(entity)
            }
        }
    }

    func read(key: String) -> String? {
        let realm = try! Realm()
        let result = realm.objects(RealmPersistedEntity.self).filter("key == %@", key)
        return result.first?.value
    }
}
