//
//  UserDefaultsProvider.swift
//  Persistence
//
//  Created by Tomáš Pařízek on 19.11.2023.
//

import Foundation

struct UserDefaultsProvider: PersistenceProvider {
    func save(key: String, value: String) {
        UserDefaults.standard.setValue(value, forKey: key)
    }

    func save(values: [String: String]) {
        for (key, value) in values {
            UserDefaults.standard.setValue(value, forKey: key)
        }
    }

    func read(key: String) -> String? {
        UserDefaults.standard.string(forKey: key)
    }
}
