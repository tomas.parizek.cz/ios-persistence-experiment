//
//  KeychainProvider.swift
//  Persistence
//
//  Created by Tomáš Pařízek on 19.11.2023.
//

import Foundation
import KeychainSwift

struct KeychainProvider: PersistenceProvider {
    let keychain = KeychainSwift()

    func save(key: String, value: String) {
        keychain.set(value, forKey: key)
    }

    func save(values: [String: String]) {
        for (key, value) in values {
            keychain.set(value, forKey: key)
        }
    }

    func read(key: String) -> String? {
        keychain.get(key)
    }
}
