//
//  PersistenceApp.swift
//  Persistence
//
//  Created by Tomáš Pařízek on 19.11.2023.
//

import SwiftUI

@main
struct PersistenceApp: App {
    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
